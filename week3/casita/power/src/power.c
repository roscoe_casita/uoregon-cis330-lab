/*
 ============================================================================
 Name        : power.c
 Author      : Roscoe Casita
 Version     :
 Copyright   : University of Oregon
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

float power(float a, float b)
{
	float returnValue = 1.0;
	while(b >= 1.0)
	{
		returnValue = returnValue * a ;
		b = b - 1.0;
	}
	return returnValue;
}

float average(float a, float b)
{
	return (a + b) / 2;
}

float execute(float (*func)(float a, float b), float *ptr)
{
	return (*func)(ptr[0], ptr[1]);

}

int main(void)
{
	float (*func)(float a, float b);
	float items[] = {2.0,2.0};
	float *items_ptr = items;

	func = power;
	float result = execute(func,items_ptr);
	printf("Power function execute dynamic: %f\r\n",result);


	func = average;
	result = execute(func,items_ptr);
	printf("Average function execute dynamic: %f\r\n",result);

	void *danger = (void*)power;

	float *bad = (float *)danger;


	result = execute(func,bad);
	printf("Average function execute dynamic: %f\r\n",result);


	printf("Before writing to bad code address float pointer.\r\n");

	(*bad) = 100.1;

	return EXIT_SUCCESS;
}
