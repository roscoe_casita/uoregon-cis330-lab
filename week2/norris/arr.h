#ifndef __ARR_H
#define __ARR_H

void create(int ***x, int n, int m);
void init(int **x, int n, int m, int val);
void print(int **x, int n, int m);
void del(int **x, int n, int m);

#endif
