#include <stdlib.h>
#include <stdio.h>
#include "arr.h"

void create(int ***x, int n, int m) {
   (*x) = (int **)  malloc (n * sizeof(int *));
   for (int i = 0; i < n; i++) {
       (*x)[i] = (int *) malloc ( m * sizeof(int));
   }
}

void init(int **x, int n, int m, int val) {
   for (int i = 0; i < n; i++) 
	   for (int j = 0; j < m; j++) 
		   x[i][j] = val;
}

void print(int **x, int n, int m) {
   for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) 
	   printf("%d ", x[i][j]);
   	printf("\n");
   }
}

void del(int **x, int n, int m) {
}

