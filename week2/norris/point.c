struct PointStruct {
    int x;
    int y;
};
typedef struct PointStruct Point;

void setPoint(Point* p, int a, int b) {
   (*p).x = a;
   p->y = b;
} 

int main() {
 
   Point pt; 
   setPoint(&pt, 2, 4);
   return 0;
}
