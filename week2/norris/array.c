#include <stdio.h>

#define N 2
#define M 4

int main() {
    int array[N][M];
    int i, j, k;
    for (i = 0; i < N; i++) {
        for (j = 0; j < M; j++) {
            array[i][j] = j;
            printf("%d ", array[i][j]);
        }
        printf("\n");
    }
}