#include <stdio.h>
void increment (int *x) {
	(*x)++;
}
int main() {
	int y = 2;
	printf("y = %d\n", y);
	increment (&y);
	printf("After increment: y = %d\n", y);
	return 0;
}
