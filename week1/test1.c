#include <stdio.h>

int main() {
	
	const int maxSize = 32;
	int firstDigit;

	char userInput[maxSize];

	fgets( userInput, maxSize, stdin );

	if ( userInput[0] >= '0' && userInput[0] <= '9' ) {
		firstDigit = userInput[0] - '0';
		printf("The first digit is %d\n", firstDigit); 
	}
	return 0;
}
